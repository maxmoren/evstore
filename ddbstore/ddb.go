package ddbstore

import (
	"bitbucket.org/sn0e/evstore"
	"context"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/kinesis"
)

type EvDDB struct {
	db *dynamodb.DynamoDB
}

func New(k *kinesis.Kinesis) *EvDDB {
	return &EvDDB{db: db}
}

func (evddb *EvDDB) Read(ctx context.Context, streamId string, since int64, count int, types map[string]func() interface{}) ([]evstore.Item, int64, bool, error) {
	return nil, 0, false, nil
}

func (evddb *EvDDB) Commit(ctx context.Context, streamId string, version int64, events ...evstore.Event) error {
	k.PutRecordsWithContext()
}
