package evstore

import (
	"testing"
	"context"
	"bitbucket.org/sn0e/evstore/storeimpl"
)

type testEvent struct {
	Amount int
}

func (testEvent) Type() string {
	return "Increment"
}

type sum struct {
	Emitter
	number int
}

func (s *sum) EventTypes() map[string]func()interface{} {
	return map[string]func()interface{}{
		testEvent{}.Type(): func()interface{}{ return &testEvent{}},
	}
}

func (s *sum) HandleEvent(e interface{}, _ Meta) {
	if te, ok := e.(*testEvent); ok {
		s.number = s.number + te.Amount
	}
}

func (s *sum) Increment() {
	s.Emit(testEvent{Amount: 1})
}

func TestReplay(t *testing.T) {
	testStore := NewSliceStore()
	testStore.Commit(context.TODO(), "test-entity", 0,
		storeimpl.Event{TypeName:"Increment", Event: testEvent{Amount: 5}},
		storeimpl.Event{TypeName:"Increment", Event: testEvent{Amount: 10}},
	)

	s := &sum{}
	_, exists, err := Entity(context.TODO(), testStore, "test-entity", s)
	if err != nil {
		t.Fatal(err)
	}
	if !exists {
		t.Fatal("expected exists == true")
	}
	if s.number != 15 {
		t.Fatal("expected s.number == 15, got", s.number)
	}
}

func TestEmit(t *testing.T) {
	testStore := NewSliceStore()
	s := &sum{}
	save, exists, err := Entity(context.TODO(), testStore, "test-entity", s)
	if err != nil {
		t.Fatal(err)
	}
	if exists {
		t.Fatal("expected exists == false")
	}

	s.Increment()

	if err := save(context.TODO()); err != nil {
		t.Fatal(err)
	}
	if len(testStore.Stream("test-entity")) != 1 {
		t.Fatal("event not appended to store")
	}

	s.Increment()
	s.Increment()

	if err := save(context.TODO()); err != nil {
		t.Fatal(err)
	}
	if numEvents := len(testStore.Stream("test-entity")); numEvents != 3 {
		t.Fatal("expected len(stream) == 3, got", numEvents)
	}
	if s.number != 3 {
		t.Fatal("expected number == 3, got", s.number)
	}
}
