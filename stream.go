package evstore

import (
	"sync"
	"time"
)

type MemoryStream struct {
	subscribers []func(interface{})
	mutex       sync.RWMutex
	flush       chan bool
	flushed     chan bool
}

func NewMemoryStream() *MemoryStream {
	return &MemoryStream{
		flush:   make(chan bool),
		flushed: make(chan bool),
	}
}

func (s *MemoryStream) run(pipe <-chan interface{}, exit <-chan bool) {
	buffer := make([]interface{}, 100)
	level := 0
	for {
		notify := false
		select {
		case val := <-pipe:
			buffer[level] = val
			level++
			continue
		case <-exit:
			return
		case <-s.flush:
			notify = true
		case <-time.After(1 * time.Second):
		}
		if level > 0 {
			s.mutex.RLock()
			subs := s.subscribers[:]
			s.mutex.RUnlock()
			for _, subscriber := range subs {
				for event := range buffer[0:level] {
					subscriber(event)
				}
			}
			level = 0
		}
		if notify {
			s.flushed <- true
		}
	}
}

func (s *MemoryStream) Subscribe(subscriber func(interface{})) func() {
	s.mutex.Lock()
	idx := len(s.subscribers)
	s.subscribers = append(s.subscribers, subscriber)
	s.mutex.Unlock()
	return func() {
		s.mutex.Lock()
		s.subscribers = append(s.subscribers[0:idx], s.subscribers[idx:]...)
		s.mutex.Unlock()
	}
}

func (s *MemoryStream) Flush() {
	s.flush <- true
	<-s.flushed
}

func (s *MemoryStream) Run() (chan<- interface{}, chan<- bool) {
	pipe := make(chan interface{}, 100)
	exit := make(chan bool)

	go func() {
		s.run(pipe, exit)
		close(pipe)
		close(exit)
	}()

	return pipe, exit
}
