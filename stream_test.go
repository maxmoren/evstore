package evstore

import (
	"testing"
)

func TestStream(t *testing.T) {

	makeHandler := func() (func(interface{}), *bool) {
		called := false
		return func(_ interface{}) {
			called = true
		}, &called
	}

	h1, called1 := makeHandler()
	h2, called2 := makeHandler()

	s := NewMemoryStream()
	pipe, exit := s.Run()
	defer func() { exit <- true }()

	s.Subscribe(h1)
	s.Subscribe(h2)

	pipe <- struct{ Message string }{"Hello"}

	s.Flush()

	if !*called1 || !*called2 {
		t.Fatal("expected called1=true called2=true, was", *called1, *called2)
	}
}
