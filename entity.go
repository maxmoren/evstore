// Event store library
//
//
package evstore

import (
	"context"
	"time"
	"bitbucket.org/sn0e/evstore/storeimpl"
	"reflect"
)

type Emitter struct {
	Emit func(...Event) `json:"-"`
}

func (em *Emitter) setEmit(f func (...Event)) {
	em.Emit = f
}

type emitReceiver interface {
	setEmit(func (...Event))
}

type EventHandler interface {
	emitReceiver

	// EventTypes should return a map of name => factory for each understood event type.
	EventTypes() map[string]func()interface{}

	// HandleEvent is called with all events for a stream when replayed or when emitting new events.
	HandleEvent(event interface{}, meta Meta)
}

type Meta struct {
	ID        string
	Timestamp time.Time
	EventID   string
	Version   int64
}

type entity struct {
	id          string
	version     int64
	uncommitted []storeimpl.Event
	store       storeimpl.Store
	handler     EventHandler
}

// TODO: Replace or disable this for production.
func assertPointer(v Event) {
	if reflect.ValueOf(v).Kind() != reflect.Ptr {
		panic("only pointer types can be emitted")
	}
}

func (e *entity) emit(events ...Event) {
	if e.id == "" {
		panic("not initialized")
	}
	for _, t := range events {
		assertPointer(t)
		e.handler.HandleEvent(t, Meta{ID: e.id})
		e.uncommitted = append(e.uncommitted, storeimpl.Event{TypeName: t.Type(), Event: t})
	}
}

func MustStore(s storeimpl.Store, err error) storeimpl.Store {
	if err != nil {
		panic(err)
	}
	return s
}

func (e *entity) save(ctx context.Context) error {
	if e.id == "" {
		panic("not initialized")
	}
	if len(e.uncommitted) == 0 {
		return nil
	}
	err := e.store.Commit(ctx, e.id, e.version, e.uncommitted...)
	if err != nil {
		return err
	}
	e.version += int64(len(e.uncommitted))
	e.uncommitted = nil
	return nil
}

// Entity loads an Entity from
func Entity(ctx context.Context, store storeimpl.Store, id string, h EventHandler) (save func (context.Context) error, exists bool, err error) {
	var evs []storeimpl.Item
	e := &entity{id: id, store: store}
	h.setEmit(e.emit)
	e.handler = h
	version := e.version

	for hasMore := true; hasMore; {
		evs, version, hasMore, err = store.Read(ctx, id, version, 100, h.EventTypes())
		if err != nil {
			return nil, false, err
		}
		for _, ev := range evs {
			meta := Meta{
				ID:        ev.StreamID,
				EventID:   ev.EventID,
				Timestamp: ev.Timestamp,
				Version:   version,
			}
			h.HandleEvent(ev.Event, meta)
		}
		e.version = version
	}
	if version == 0 {
		return e.save, false, nil
	}
	return e.save, true, nil
}

type Event interface {
	Type() string
}
