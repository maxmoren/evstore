package evstore

import (
	"context"
	"github.com/google/uuid"
	"bitbucket.org/sn0e/evstore/storeimpl"
	"time"
	"fmt"
)

type SliceStore struct {
	events map[string][]storeimpl.Item
}

func NewSliceStore() *SliceStore {
	return &SliceStore{make(map[string][]storeimpl.Item)}
}

func (im *SliceStore) Stream(streamId string) []storeimpl.Item {
	return im.events[streamId]
}

func (im *SliceStore) Read(ctx context.Context, streamId string, since int64, count int, types map[string]func() interface{}) ([]storeimpl.Item, int64, bool, error) {
	if _, ok := im.events[streamId]; !ok {
		return nil, 0, false, nil
	}
	return im.events[streamId][since:count], since + int64(count), false, nil
}

type errConflict struct {
	expected, actual int64
}

func (ec errConflict) Error() string {
	return fmt.Sprintf("expected version %d, was %d", ec.expected, ec.actual)
}

func (im *SliceStore) Commit(ctx context.Context, streamId string, version int64, events ...storeimpl.Event) error {
	if len(im.events[streamId]) == 0 {
		if version != 0 {
			return errConflict{version, 0}
		}
	} else if actualVersion := im.events[streamId][len(im.events)-1].Version; actualVersion != version {
		return errConflict{version, actualVersion}
	}
	for _, e := range events {
		version++
		im.events[streamId] = append(im.events[streamId], storeimpl.Item{
			StreamID:  streamId,
			Version:   version,
			Timestamp: time.Now(),
			EventID:   uuid.Must(uuid.NewRandom()).String(),
			Event:     e.Event,
		})
	}
	return nil
}
