package pqstore

import "database/sql"

// Store is an instance of the driver.
type Store struct {
	db          *sql.DB
	dsn         string
	commitStmt  *sql.Stmt
	readStmt    *sql.Stmt
	readAllStmt *sql.Stmt
}

// New initializes a new Store for a database connection DSN.
func New(dsn string) (*Store, error) {
	db, err := sql.Open("postgres", dsn)
	if err != nil {
		return nil, err
	}
	query := `SELECT append_events($1, $2, $3::input_event[])`
	commitStmt, err := db.Prepare(query)
	if err != nil {
		return nil, err
	}

	readAllQuery := `
	     SELECT stream_id, event_type, event_data, event_offset FROM event
	      WHERE event_offset > $1
	   ORDER BY event_offset
	      LIMIT $2
	`
	readAllStmt, err := db.Prepare(readAllQuery)
	if err != nil {
		return nil, err
	}

	readQuery := `
	     SELECT stream_id, event_type, event_data, stream_version FROM event
	      WHERE stream_id = $1
	        AND stream_version > $2
	   ORDER BY stream_version
	      LIMIT $3
	`
	readStmt, err := db.Prepare(readQuery)
	if err != nil {
		return nil, err
	}

	return &Store{
		db:          db,
		dsn:         dsn,
		commitStmt:  commitStmt,
		readStmt:    readStmt,
		readAllStmt: readAllStmt,
	}, nil
}
