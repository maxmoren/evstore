package pqstore

import (
	"bytes"
	"context"
	"database/sql"
	"database/sql/driver"
	"encoding/json"
	"fmt"
	"strconv"

	"bitbucket.org/sn0e/evstore/storeimpl"
	"github.com/lib/pq"
)

type item struct {
	typeName string
	event    interface{}
}

func (e item) Value() (driver.Value, error) {
	var str bytes.Buffer

	js, err := json.Marshal(e.event)
	if err != nil {
		return nil, err
	}

	str.WriteString("(")
	str.WriteString(strconv.Quote(e.typeName))
	str.WriteString(",")
	str.WriteString(strconv.Quote(string(js)))
	str.WriteString(")")

	res := str.String()

	return res, nil
}

type errConflict struct {
	verActual, verExpected int64
}

func (err errConflict) Error() string {
	return fmt.Sprintf("expected version %d, was %d", err.verExpected, err.verActual)
}

func (err errConflict) Conflict() bool {
	return true
}

// Commit atomically persists uncommitted events for a stream.
// If the passed expected stream version does not match in the store, a conflict error is returned.
func (d *Store) Commit(ctx context.Context, streamID string, version int64, events ...storeimpl.Event) error {
	items := make([]item, len(events))
	for i, e := range events {
		items[i] = item{typeName: e.TypeName, event: e.Event}
	}
	var streamVersion sql.NullInt64
	if err := d.commitStmt.QueryRowContext(ctx, streamID, version, pq.Array(items)).Scan(&streamVersion); err != nil {
		// TODO: Wrap error
		return err
	}
	if !streamVersion.Valid {
		return errConflict{version, streamVersion.Int64}
	}
	return nil
}
