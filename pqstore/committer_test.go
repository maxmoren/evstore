package pqstore

// import (
// 	"context"
// 	"crypto/rand"
// 	"encoding/base64"
// 	"testing"

// 	"bitbucket.org/sn0e/evstore"
// )

// func (TestEvent) Type() string {
// 	return "TestEvent"
// }

// type TestEvent struct {
// 	Foo string
// }

// func randomStreamId() string {
// 	var b [18]byte
// 	rand.Read(b[:])
// 	return "test-" + base64.URLEncoding.EncodeToString(b[:])
// }

// func TestCommitReturnsOkForNewStreams(t *testing.T) {
// 	driver, err := New("dbname=gofrulle sslmode=disable")
// 	if err != nil {
// 		t.Fatal(err)
// 	}
// 	_, err = driver.Commit(context.TODO(), randomStreamId(), 0,
// 		TestEvent{Foo: "Bar"},
// 	)
// 	if err != nil {
// 		t.Fatal(err)
// 	}
// }

// func TestCommitReturnsConflict(t *testing.T) {
// 	streamId := randomStreamId()
// 	committer, err := New("dbname=gofrulle sslmode=disable")
// 	if err != nil {
// 		t.Fatal(err)
// 	}
// 	_, err = committer.Commit(context.TODO(), streamId, 0, TestEvent{Foo: "Bar"})
// 	if err != nil {
// 		t.Fatal(err)
// 	}
// 	_, err = committer.Commit(context.TODO(), streamId, 0, TestEvent{Foo: "Baz"})
// 	if err == nil {
// 		t.Fatal("expected conflict error, got nil")
// 	}
// 	if conflict, ok := err.(ErrConflict); ok {
// 		if conflict != 0 {
// 			t.Fatalf("expected conflict version to be 0, catual %d", conflict)
// 		}
// 	} else {
// 		t.Fatalf("expected conflict error, got some other error: %s", err)
// 	}
// }

// type ev struct {
// 	Foo string
// 	Bar string
// 	Baz string
// }

// func (ev) Type() string {
// 	return "FakeEventHappened"
// }

// func doCommit(numEvents int, b *testing.B) {
// 	streamId := randomStreamId()
// 	committer, err := New("dbname=gofrulle sslmode=disable")
// 	var ver int64

// 	for i := 0; i < b.N; i++ {
// 		if err != nil {
// 			b.Fatal(err)
// 		}
// 		var evs []evstore.Event
// 		for j := 0; j < numEvents; j++ {
// 			evs = append(evs, ev{Foo: "Foo", Bar: "Bar", Baz: "Baz"})
// 		}
// 		ver, err = committer.Commit(context.TODO(), streamId, ver, evs...)
// 		if err != nil {
// 			b.Fatal(err)
// 		}
// 	}
// }

// func BenchmarkCommit1(b *testing.B)   { doCommit(1, b) }
// func BenchmarkCommit2(b *testing.B)   { doCommit(2, b) }
// func BenchmarkCommit5(b *testing.B)   { doCommit(5, b) }
// func BenchmarkCommit10(b *testing.B)  { doCommit(10, b) }
// func BenchmarkCommit50(b *testing.B)  { doCommit(50, b) }
// func BenchmarkCommit100(b *testing.B) { doCommit(100, b) }
