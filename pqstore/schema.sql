CREATE TABLE event (
  event_offset bigint not null,
  stream_id text not null,
  stream_version bigint not null,
  event_time timestamptz not null,
  event_type text not null,
  event_data jsonb not null
);

-- Possibly a fast way to query for intervals of event ids as they are sequential append only.
CREATE INDEX event_offset_idx ON event USING brin (event_offset);

-- Possible problem here. This index will grow large. Sharding may help?
CREATE INDEX stream_id_idx ON event (stream_id);

-- Maybe use CACHE X here for faster inserts?
CREATE SEQUENCE event_seq CACHE 100 OWNED BY event.event_offset;
ALTER TABLE event ALTER event_offset SET DEFAULT nextval('event_seq');

CREATE VIEW stream AS
  SELECT
    stream_id,
    MAX(stream_version) AS stream_version
  FROM event
  GROUP BY stream_id;

CREATE TYPE input_event AS (
  event_type text,
  event_data jsonb
);

CREATE OR REPLACE FUNCTION append_events(i_stream_id text, i_version bigint, i_data input_event[]) RETURNS bigint AS $$
DECLARE
  stream_version bigint;
BEGIN
  WITH inserted (new_stream_version) AS (
    INSERT INTO event (stream_id, stream_version, event_time, event_type, event_data)
      SELECT
        i_stream_id,
        new_data.ord + coalesce(stream.stream_version, 0) AS new_stream_version,
        'now'::timestamptz,
        new_data.event_type,
        new_data.event_data
      FROM unnest(i_data) WITH ORDINALITY AS new_data (event_type, event_data, ord)
        LEFT JOIN stream ON stream_id = i_stream_id
      WHERE (i_version = 0 AND stream.stream_version IS NULL) OR i_version = stream.stream_version
      ORDER BY new_data.ord
    RETURNING event.stream_version
  ) SELECT MAX(inserted.new_stream_version) FROM inserted INTO stream_version;
  RETURN stream_version;
END
$$ LANGUAGE plpgsql VOLATILE;
