package storeimpl

import (
	"time"
	"context"
)

type Item struct {
	Event     interface{}
	StreamID  string
	EventID   string
	Timestamp time.Time
	Version   int64
}

type Event struct {
	TypeName string
	Event    interface{}
}

type Store interface {
	Commit(ctx context.Context, streamId string, version int64, events ...Event) error
	Read(ctx context.Context, streamId string, since int64, count int, types map[string]func() interface{}) ([]Item, int64, bool, error)
}