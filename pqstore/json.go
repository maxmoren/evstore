package pqstore

import (
	"encoding/json"
	"errors"
)

type jsonTypeMap map[string]func() interface{}

func (tm jsonTypeMap) Unmarshal(typeName string, j []byte) (interface{}, error) {
	if factory, ok := tm[typeName]; ok {
		dest := factory()
		err := json.Unmarshal(j, dest)
		if err != nil {
			return nil, err
		}
		return dest, nil
	}
	return nil, errors.New("unknown type")
}
