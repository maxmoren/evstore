package pqstore

import (
	"context"
	"database/sql"
	"strconv"
	"time"

	"bitbucket.org/sn0e/evstore/storeimpl"
)

func (d *Store) readFromRows(ctx context.Context, rows *sql.Rows, since int64, count int, types map[string]func() interface{}) ([]storeimpl.Item, int64, bool, error) {
	var events []storeimpl.Item
	var totalRows int
	var last = since
	typeMap := jsonTypeMap(types)
	err := func() error {
		for rows.Next() {
			totalRows++
			var streamID string
			var eventData []byte
			var eventType string
			var eventTime time.Time
			var version int64
			err := rows.Scan(&streamID, &eventType, &eventData, &version)
			last = version
			if err != nil {
				return err
			}
			v, err := typeMap.Unmarshal(eventType, eventData)
			if err != nil {
				return err
			}
			events = append(events, storeimpl.Item{
				Event:     v,
				EventID:   strconv.FormatInt(version, 10),
				StreamID:  streamID,
				Timestamp: eventTime,
				Version:   version,
			})
		}
		return nil
	}()
	if err != nil {
		return nil, 0, false, err
	}
	return events, last, totalRows > count, nil
}

// ReadAll returns events for all streams.
func (d *Store) ReadAll(ctx context.Context, since int64, count int, types map[string]func() interface{}) ([]storeimpl.Item, int64, bool, error) {
	rows, err := d.readAllStmt.QueryContext(ctx, since, count+1)
	if err != nil {
		return nil, 0, false, err
	}
	return d.readFromRows(ctx, rows, since, count, types)
}

// Read returns events for a stream.
func (d *Store) Read(ctx context.Context, streamID string, since int64, count int, types map[string]func() interface{}) ([]storeimpl.Item, int64, bool, error) {
	rows, err := d.readStmt.QueryContext(ctx, streamID, since, count+1)
	if err != nil {
		return nil, 0, false, err
	}
	return d.readFromRows(ctx, rows, since, count, types)
}
